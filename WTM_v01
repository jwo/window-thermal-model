#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: Hans Oosterbeek
Date: 17.11.2020

WTM.py (Window Thermal Model)


Room temperature at 293 K


"""

from scipy.constants import pi
from scipy.constants import sigma as sig, c # Stefan-Boltzmann costant [W/(m^2K^4)] and c [m/s]
import numpy as np
import matplotlib.pyplot as plt

plt.close('all')


## constants
TK = 273 # zero degrees C in K


## Temperatures
T_start = 51 # start temperature of body in deg. C. Obtain from an initial data plot
T0 = TK + T_start # T_start in K
T_sur = 35 # Temperature [degree C] of window surroundings (compound of vessel, outside vessel and cuff)
Ts = TK + T_sur # T_sur in K

## Frequency
f = 140E9


## Fitparameters
eps = 0.88 # Emissivity [-]. Obtain from fit of cooling down phase, but note there is also conduction! (see below)
h = 6 # Newton heat transfer coefficient [W/(m^2 * K)] to add some convection (disk facing air side).
p = 50E3 # 47.8 kW/m^2 (center), with R = 0.1 and A = 0.05 (assuming a td!) giving 55 kW/m^2 effective into the disk
pd = p # pd in order to recall the load again in case of multi-pulse (load is set to zero in off-phase)

## Plasma heating data
p_p = 0 # W/m^2
pd_p = p_p # p_pd in order to recall the load again in case of multi-pulse (load is set to zero in off-phase)


## Disk data (should not matter: volume absorber)
erp = 3.8 # permittivity
td = 1.5E-3 # loss tangent. Note: it is higher for modelling the range 20 .. 40 degrees (used in the experiment)
r = (89/2)*1E-3 # radius [m]
d = 6E-3 # thickness [m]
S = pi*r**2 # Overall exposed surface [m^2]
V = S * d # Volume [m^3]
cp = 715 # Specific Heat Capacity [J/(kg*K)] at room temperature, Heraeus data sheet
rho = 2.2E3 # Specific density [kg/m^3], Heraeus data sheet
m = rho * V # calculated from above: mass


## Optional: Data to fit the model to  
DAQ_name = '20200520_07_AI0.npy'

# Scaling
T_w = 152 # measured immediately after heating off with surface probe
V_py = 1.07795 # recorded voltage at heating off
k_py = T_w/V_py # scaling pyrometer


## Time vector and heating on/ off phase (to match DAQ data)
N_p = 3 # Number of pulses, one pulse is: heating on period + heating off period
dt = 0.1 # time increament model
H_on = 1565 # 1566 Duration of heating on-time. Find from plot of data
H_off = 500 # Duration of heating off-time. Find from plot of data
N = int(H_on/dt) # number of points to calculate during heating on
M = int(H_off/dt) # number of points to calculate after the heating on
t = np.linspace(0, N_p*(N+M-1), N_p*(N+M)) # Time vector in dt incremenst
ts = tuple([i*dt for i in t]) # Time vector in s incremenst

j = 1
T_wNK = np.zeros(0)
while j < N_p+1:
	## Compute delta T
	T = T0  # start temperature
	T_w_on = np.zeros(N) # array with window temperature during heating on time
	T_w_on[0] = T
	i = 1  
	while i < N:
		num1 = p*pi*f*td*(1+erp)
		den1 = cp*rho*c
		num2 = 2*eps*sig*(T**4 - Ts**4)
		den2 = cp*rho*d
		num3 = h*(T-Ts)
		den3 = cp*rho*d
		num4 = p_p*S
		den4 = cp*m
		dT = (num1/den1)*dt - (num2/den2)*dt - (num3/den3)*dt + (num4/den4)*dt
		T = T + dT
		T_w_on[i] = T
		i = i +1
	
	## Compute delta T, heating off-time
	p = 0; p_p = 0 # gstray radiation load off, plasma load off
	T_start = T_w_on[int((H_on/dt) -1)] # temperature as heating  stopped
	T = T_start
	T_w_off = np.zeros(M) ## array with bolometer temperature during heating off time
	T_w_off[0] = T
	i = 1  
	while i < M:
		num1 = p*pi*f*td*(1+erp)
		den1 = cp*rho*c
		num2 = 2*eps*sig*(T**4 - Ts**4)
		den2 = cp*rho*d
		num3 = h*(T-Ts)
		den3 = cp*rho*d
		num4 = p_p*S
		den4 = cp*m
		dT = (num1/den1)*dt - (num2/den2)*dt - (num3/den3)*dt + (num4/den4)*dt
		T = T + dT
		T_w_off[i] = T
		i = i +1
	T_wK = np.append(T_w_on, T_w_off) # one complete cycle
	T_wNK = np.append(T_wNK, T_wK) # adding cycles
	T0 = T_wNK[-1]
	p = pd; p_p = pd_p # set loads for the next pulse
	j = j +1
T_w = tuple([i-273 for i in T_wNK]) # convert array to degrees C


fig = plt.figure(1)
plt.plot(ts,T_w, label = 'Model: loss tan, radiation, convection')
## compare to experiment data
DAQ = np.load(DAQ_name)
DAQ_size = np.size(DAQ)
dt_d = 0.1 # [s] Sample period time data ADC, set by hand (for now). Compute or get from data.properties[wf-increment] in the tdms-file
t_DAQ = np.linspace(0, DAQ_size*dt_d, DAQ_size)
leg_str = ('Data: ' + DAQ_name)
plt.plot(t_DAQ, DAQ*k_py, label = leg_str)
plt.ylim(0, 200)
plt.xlabel('t [s]')
plt.ylabel('T [$^{\circ}$C]')
plt.legend(loc='lower center')
plt.grid()
t_str = ('Window Thermal Model: Vacom SiO$_2$ window')
plt.title(t_str)
p = int(pd/1000)
plt_str = 'p = ' + str(p) + ' kW/m$^{2}$' +  r', tan $\delta$ = '+str(td)+ ' [-]' +', $\epsilon$ = ' + str(eps) + ' [-]' + \
 	', h = ' + str(h) + ' W/(m$^2$*K)'
plt.text(0, 190, plt_str)
plt_str = 'c$_p$ = ' + str(cp) + ' J/(kg*K)'
plt.text(0, 175, plt_str)
plt_str = r'$\rho$ = ' + str(int(rho)) + ' kg/m$^3$'
plt.text(2500, 175, plt_str)
plt.show()